#include <stdio.h>

typedef void(*vmethod)(void* );
vmethod *m_vtable;
vmethod *v_vtable;
typedef struct MStruct_{
	int rows;
	int cols;
	int *matrix_arr;
	void(*mptr_mult)(MStruct, MStruct);
	void(*mptr_add)(MStruct, MStruct);
	void(*mptr_norm)(MStruct);
	vmethod *vtable_ptr;
}MStruct;

typedef struct VStruct_{
	MStruct ms;
	int max;
	int min;
	void(*vptr_norm)(VStruct);
	vmethod *vtable_ptr;
}VStruct;

void matrixMult(MStruct *m1, MStruct *m2){
	if (m1->rows == m2->cols)
	{
		int cols = m1->rows;
		int rows = m2->cols;
		MStruct *m3 = malloc(sizeof(MStruct));
		create(m3, rows, cols);
		int a=0;
		int x,y,z,i;
		for(a =0; a < (cols*rows); a++)
		{
			m3->matrix_arr[a] = 0;
		}				
		for (x = 0; x < rows; x++) 
		{
			for (y = 0; y < cols; ++y) 
			{
				for (z = 0; z < m1->cols; ++z) 
				{
					m3->matrix_arr[x*cols+y] += m1->matrix_arr[x*(m1->cols)+z] * m2->matrix_arr[z*(cols)+y];
				}
			}
		}
		printf("The product of two matrices is:\n");
		for (i = 0; i < (cols*rows); ++i)
		{
			printf("%d	",m3->matrix_arr[i]);
			if (i % cols == (cols - 1)) 
			{
				printf("\n");
			}
		}
	}
	else
	{
		printf("Cannot perform matrix multiplication.\n");
	}
}

void matrixAdd(MStruct *m1, MStruct *m2){
	if ((m1->rows == m2->rows) && (m1->cols == m2->cols))
	{
		printf("The sum of two matrices is:\n");
		int i = 0;
		for (i = 0; i<(m1->rows*m1->cols); i++){
			m1->matrix_arr[i] += m2->matrix_arr[i];
			printf("%d", m1->matrix_arr[i]);
			printf("\n");
		}
	}
	else
	{
		printf("Cannot perform matrix addition.\n");
	}
}

void vectorAdd(VStruct *v1, VStruct *v2){
	if (v1->ms.cols == v2->ms.cols)
	{
		printf("The sum of two vectors is:\n");
		int i = 0;
		for (i = 0; i<(v1->ms.cols); i++){
			v1->ms.matrix_arr[i] += v2->ms.matrix_arr[i];
			printf("%d", v1->ms.matrix_arr[i]);
			printf("\n");
		}
	}
	else
	{
		printf("Cannot perform vector addition.\n");
	}
}

void matrixL1Norm(MStruct *m1){
	int h=0;int i=0;int col=0; int j=0; int sum=0; int max=0;
for(h=0; h<m1->cols;h++){
	for(j=0; j<m1->rows; j++)
	{
		sum+=m1->matrix_arr[j*m1->cols+h];	
	}
	if(sum > max)
	{
		col ++;
		max=sum;
	}
	sum=0; 
}
	printf("The maximum sum is: %d in the column %d\n",max, col);
}

void vectorL1Norm(VStruct *v1){
	int i=0;
	int max = v1->max;
	for(i=0; i<v1->ms.cols; i++)
	{
		max+= v1->ms.matrix_arr[i];
	}
	printf("The value of L1Norm is: %d\n", max);
}

void *init_m(MStruct *mptr, int rows, int cols){
	mptr->rows = rows;
	mptr->cols = cols;
	mptr->matrix_arr = malloc(sizeof(rows*cols));
	mptr->mptr_mult = &matrixMult;
	mptr->mptr_add = &matrixAdd;
	int i;
	printf("The matrix is:\n");
	for (i = 0; i <(mptr->rows*mptr->cols); i++)
	{
		mptr->matrix_arr[i] = rand()%50;
		printf("%d	", mptr->matrix_arr[i]);
		if (i % cols == (cols - 1)) 
		{
			printf("\n");
		}
	}
	return mptr;
}

void *init_v(VStruct *vptr, int cols){
	vptr->ms.rows = 1;
	vptr->ms.cols = cols;
	vptr->ms.matrix_arr = malloc(sizeof(cols));
	vptr->max = 0;
	vptr->min = 0;
	vptr->vptr_norm = &vectorL1Norm;
	vptr->ms.mptr_mult = &matrixMult;
	vptr->ms.mptr_add = &vectorAdd;
	int i;
	printf("The vector is:\n");
	for (i = 0; i <vptr->ms.cols; i++)
	{
		vptr->ms.matrix_arr[i] = rand()%50;
		printf("%d\n", vptr->ms.matrix_arr[i]);
	}
	return vptr;
}

void create(MStruct *mptr, int rows, int cols)
{
	init_m(mptr, rows, cols);
}

int main(void) {
	int choice1; int choice2;
	printf("Select whether you want to perform vector operations or matrix operations.\n");
	printf("Press 1 for vector operations.\nPress 2 for matrix operations.\n");
	scanf("%d",&choice1);
	if(choice1 == 1)
	{
		printf("~ ~ ~ Vector Operations ~ ~ ~\n");
		printf("Press 1 for vector L1Norm.\nPress 2 for vector addition.\n");
		scanf("%d",&choice2);
		if(choice2== 1)
		{
			v_vtable = malloc(sizeof(vmethod));
			VStruct *vptr = malloc(sizeof(VStruct));
			v_vtable[0] = &vectorL1Norm;
			init_v(vptr, 4);
			vptr->vtable_ptr = v_vtable;
			vptr->vtable_ptr[0](vptr);
		}
		else if(choice2 == 2)
		{
			VStruct *vptr = malloc(sizeof(VStruct));
			VStruct *vptr1 = malloc(sizeof(VStruct));
			init_v(vptr, 4);
			init_v(vptr1, 4);
			vptr->ms.mptr_add(vptr, vptr1);
		}
		else
		{
			printf("Incorrect option selected.\n");
		}
	}	
	else if(choice1 == 2)
	{
		printf("~ ~ ~ Matrix Operations ~ ~ ~\n");
		printf("Press 1 for matrix L1Norm.\nPress 2 for matrix addition.\nPress 3 for matrix multiplication.\n");
		scanf("%d",&choice2);
		if(choice2 == 1)
		{
			m_vtable = malloc(sizeof(vmethod));
			MStruct *m1ptr = malloc(sizeof(MStruct));
			m_vtable[0] = &matrixL1Norm;
			init_m(m1ptr, 2, 3);
			m1ptr->vtable_ptr = m_vtable;
			m1ptr->vtable_ptr[0](m1ptr);
		}
		else if(choice2 == 2)
		{
			MStruct *m1ptr = malloc(sizeof(MStruct));
			MStruct *m2ptr = malloc(sizeof(MStruct));
			init_m(m1ptr, 2, 3);
			init_m(m2ptr, 3, 2);
			m1ptr->mptr_add(m1ptr, m2ptr);
		}
		else if(choice2 == 3)
		{
			MStruct *m1ptr = malloc(sizeof(MStruct));
			MStruct *m2ptr = malloc(sizeof(MStruct));
			init_m(m1ptr, 2, 3);
			init_m(m2ptr, 3, 2);
			m1ptr->mptr_mult(m1ptr, m2ptr);
		}
		else
		{
			printf("Incorrect option selected.\n");
		}
	}
	else{
		printf("Incorrect option selected.\n");
	}	
	return 0;
}

